# Color Changer

Allows you to change the color of your controllers in software.

## How to build

```
cargo build --profile release
```

## How to use

After building run:

```
target/release/joyconchange
```

If you get "Permission denied", you might need `sudo`.

## License

Licensed under the MIT License or, at your option, Apache License 2.0.

