use std::{fmt::Display, fs::File, error::Error, io::{BufWriter, Write}};

use colors_transform::Rgb;
use controller::{Controller, ControllerType};
use enum_iterator::{all, Sequence};
use inquire::{Select, validator::Validation, Text};
use spinner::SpinnerBuilder;

use crate::controller::{ControllersApi, color::{ControllerColors, Color}};

mod lazy;
mod controller;

#[derive(Copy, Clone, Sequence)]
enum Actions {
    DumpSPI,
    ReadColors,
    ChangeColors,
}

impl Display for Actions {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(match self {
            Actions::DumpSPI => "Dump SPI",
            Actions::ReadColors => "Read colors",
            Actions::ChangeColors => "Change colors",
        })
    }
}

fn main() {
    let sp = SpinnerBuilder::new("Searching for controllers".into()).start();

    let api = ControllersApi::new();
    let controllers = api.find_controllers();

    sp.message(format!("Found {} controller(s)", controllers.len()));
    sp.close();

    if controllers.len() == 0 {
        return;
    }

    let selected_controller =
        Select::new("Choose a controller", controllers).prompt().unwrap();

    let selected_action =
        Select::new("What would you like to do?", all::<Actions>().collect()).prompt().unwrap();

    match selected_action {
        Actions::DumpSPI => {
            dump_spi(&selected_controller)
        },
        Actions::ReadColors => {
            init_controller(&selected_controller);
            println!("Colors: {}", selected_controller.read_colors())
        },
        Actions::ChangeColors => {
            write_colors(&selected_controller);
        },
    }
}

fn init_controller(controller: &Controller) {
    let sp = SpinnerBuilder::new("Initializing controller".into()).start();

    controller.init();

    sp.message(
        format!(
            "Controller has been initialized, serial number: {}",
            controller.read_serial_number()
        )
    );
    sp.close();
}

fn dump_spi(controller: &Controller) {
    let dump_path = Text::new("Path for dump file to create").prompt();

    match dump_path {
        Ok(dump_path) => {
            let dump_file = File::create(dump_path).unwrap();
            let mut buf_writer = BufWriter::new(dump_file);
            init_controller(controller);
            let sp = SpinnerBuilder::new("Dumping SPI".into()).start();
            let mut byte_count = 0;
            controller.spi_data().for_each(|bytes| {
                buf_writer.write(bytes.as_slice()).unwrap();
                byte_count += bytes.len();
                if byte_count % (bytes.len() * 10) == 0 {
                    sp.update(format!("Dumping SPI ({} bytes dumped)", byte_count));
                }
            });
            sp.message(format!("SPI dump successful ({} bytes)", byte_count));
            sp.close();
        }
        Err(err) => println!("Error: {}", err)
    }
}

fn write_colors(controller: &Controller) {
    init_controller(&controller);
    let current_colors = controller.read_colors();

    let hex_color_validator = |input: &str| -> Result<Validation, Box<dyn Error + Send + Sync>> {
        if Rgb::from_hex_str(input).is_ok() {
            Ok(Validation::Valid)
        } else {
            Ok(Validation::Invalid(format!("Invalid color {}", input).into()))
        }
    };

    let body_hex_color = Text::new("Body color (hex)")
        .with_validator(hex_color_validator)
        .with_default(format!("{}", current_colors.body).as_str())
        .prompt()
        .unwrap();

    let sticks_hex_color = Text::new("Sticks/Buttons color (hex)")
        .with_validator(hex_color_validator)
        .with_default(format!("{}", current_colors.sticks).as_str())
        .prompt()
        .unwrap();

    let left_grip_hex_color = if controller.controller_type() == ControllerType::ProController {
        Some(Text::new("Left grip color (hex)")
            .with_validator(hex_color_validator)
            .with_default(format!("{}", current_colors.left_grip).as_str())
            .prompt()
            .unwrap())
    } else {
        None
    };

    let right_grip_hex_color = if controller.controller_type() == ControllerType::ProController {
        Some(Text::new("Right grip color (hex)")
            .with_validator(hex_color_validator)
            .with_default(format!("{}", current_colors.right_grip).as_str())
            .prompt()
            .unwrap())
    } else {
        None
    };

    let sp = SpinnerBuilder::new("Changing color".into()).start();
    controller.write_colors(
        ControllerColors::new(
            Color::from_hex_code(body_hex_color.as_str()).unwrap(),
            Color::from_hex_code(sticks_hex_color.as_str()).unwrap(),
            Color::from_hex_code(left_grip_hex_color.unwrap_or("#000000".into()).as_str()).unwrap(),
            Color::from_hex_code(right_grip_hex_color.unwrap_or("#000000".into()).as_str()).unwrap(),
            controller.controller_type(),
        )
    );
    sp.message("Colors have been written".into());
    sp.close();
}
