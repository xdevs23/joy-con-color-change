use std::rc::Rc;

use once_cell::sync::OnceCell;


pub struct LateInit<T> {
    cell: OnceCell<Rc<T>>
}

impl<T> LateInit<T> {
    pub fn init(&self, value: T) {
        assert!(self.cell.set(Rc::new(value)).is_ok())
    }
}

impl<T> Default for LateInit<T> {
    fn default() -> Self { LateInit { cell: OnceCell::default() } }
}

impl<T> std::ops::Deref for LateInit<T> {
    type Target = T;
    fn deref(&self) -> &T {
        self.cell.get().unwrap()
    }
}

impl<T> LateInit<T> {
    pub fn get(&self) -> Option<Rc<T>> {
        match self.cell.get() {
            Some(rc) => Some(rc.clone()),
            None => None,
        }
    }
}

