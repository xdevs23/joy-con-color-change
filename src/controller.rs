use std::{fmt::Display, rc::Rc, sync::atomic::{AtomicU8, Ordering, AtomicBool}, time::Duration, thread};

use enum_iterator::{all, Sequence};
use hidapi::{HidApi, DeviceInfo, BusType, HidDevice};

use crate::lazy::LateInit;

use self::{commands::{
    SPIRead, SPIReadResponse, VibrationEnable, IMUDataEnable, BluetoothDataRateIncrease, SPIWrite
}, color::ControllerColors};

mod commands;
pub mod color;

// Following code was used as reference for the protocol details:
// https://github.com/ggliv/ns_controller_colorchange

const VENDOR_ID: u16 = 0x057e;

const RUMBLE_BASE: [u8; 8] = [
    0x00, 0x01, 0x40, 0x40,
    0x00, 0x01, 0x40, 0x40,
];

#[repr(u16)]
#[derive(Copy, Clone, Debug, PartialEq, Sequence)]
pub enum ControllerType {
    JoyConLeft = 0x2006,
    JoyConRight = 0x2007,
    ProController = 0x2009,
    JoyConChargingGrip = 0x200e
}

impl ControllerType {
    fn all() -> Vec<ControllerType> {
        all::<ControllerType>().collect()
    }
}

impl Display for ControllerType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(
            match self {
                ControllerType::JoyConLeft => "Left Joy-Con",
                ControllerType::JoyConRight => "Right Joy-Con",
                ControllerType::ProController => "Pro Controller",
                ControllerType::JoyConChargingGrip => "Joy-Con Charging Grip",
            }
        )
    }
}

pub trait ControllerCommandResponse<C : ControllerCommand<Self>> : Sized {
    fn from_bytes(command: &C, bytes: Vec<u8>) -> Self;
}

pub trait ControllerCommand<R : ControllerCommandResponse<Self>> : Sized {
    fn command_bytes(&self) -> Vec<u8>;
}

pub trait ControllerSubCommandResponse<C : ControllerSubCommand<Self>> : Sized {
    fn from_bytes(command: &C, bytes: Vec<u8>) -> Self;
}

pub trait ControllerSubCommand<R : ControllerSubCommandResponse<Self>> : Sized {
    fn command_bytes(&self) -> Vec<u8>;
}

#[derive(Clone)]
pub struct Controller {
    pub vendor_id: u16,
    pub product_id: u16,
    pub manufacturer: String,
    pub product: String,
    pub serial_number: String,
    pub is_bluetooth: bool,
    pub has_initialized: Rc<AtomicBool>,
    pub path: Option<String>,
    hidapi: Rc<HidApi>,
    device: Rc<LateInit<HidDevice>>,
    subcommand_id: Rc<AtomicU8>,
}

impl Display for Controller {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!(
            "{}: {} {} ({:04x}:{:04x}, {}) via {}",
            self.controller_type(),
            self.manufacturer, self.product,
            self.vendor_id, self.product_id,
            self.serial_number,
            if self.is_bluetooth { "Bluetooth" } else { "USB" }
        ))?;
        if self.path.is_some() {
            f.write_fmt(format_args!(" [{}]", self.path.as_ref().unwrap()))?;
        }
        Result::Ok(())
    }
}

impl Controller {
    fn from(info: &DeviceInfo, hidapi: Rc<HidApi>) -> Self {
        Controller {
            vendor_id: info.vendor_id(),
            product_id: info.product_id(),
            manufacturer: info.manufacturer_string().unwrap_or("Unknown Manufacturer").to_string(),
            product: info.product_string().unwrap_or("Unknown Product").to_string(),
            serial_number: info.serial_number().unwrap().to_string(),
            path: info.path().to_str().and_then(|s| Ok(s.to_string())).ok(),
            is_bluetooth: match info.bus_type() {
                BusType::Bluetooth => true,
                BusType::Usb => false,
                _ => panic!("Unsupported bus type {:?}", info.bus_type())
            },
            hidapi,
            device: Rc::new(LateInit::default()),
            subcommand_id: Rc::new(AtomicU8::new(0)),
            has_initialized: Rc::new(AtomicBool::new(false)),
        }
    }

    pub fn controller_type(&self) -> ControllerType {
        *ControllerType::all().iter()
            .find(|&&ct| ct as u16 == self.product_id)
            .unwrap()
    }

    fn device(&self) -> Rc<HidDevice> {
        match self.device.get() {
            Some(device) => device,
            None => {
                let device = match self.hidapi.open_serial(
                    self.vendor_id,
                    self.product_id,
                    self.serial_number.as_str()
                ) {
                    Ok(device) => device,
                    Err(e) => panic!("Failed to obtain device: {}", e)
                };
                self.device.init(device);
                self.device.get().unwrap()
            }
        }
    }

    fn exchange(&self, data: Vec<u8>) -> Vec<u8> {
        loop {
            let max_len = data.len().max(256);
            //println!("HID send {:?}", data.hex_dump());
            let mut final_data = data.clone();
            if self.is_bluetooth {
                final_data.insert(0, 0x01);
            } else {
                final_data.insert(0, 0x00);
            }
            self.device().write(final_data.as_slice()).unwrap();
            let mut result = vec![0; max_len];
            let buf = result.as_mut_slice();
            let read_result = self.device().read(buf);
            let response = result[..read_result.unwrap()].to_vec();
            //println!("HID receive {:?}", response.hex_dump());
            if self.is_bluetooth {
                if response.len() == max_len {
                    // It shouldn't take this much space. Assume error. Try again.
                    continue
                }
                return response
            } else {
                // The communication is kinda flakey, so if the sending data starts
                // with 0x80, the response needs to be 0x81 and the second byte should
                // match between the two
                if data.len() < 2 || response.len() < 2 {
                    return response
                }
                if data[0] == 0x80 {
                    if response[0] == 0x81 && data[1] == response[1] {
                        return response
                    }
                } else {
                    return response
                }
            }
        }
    }

    fn attach_usb_prelude(to: Vec<u8>) -> Vec<u8> {
        let mut new_data = vec![0x80, 0x92, 0x00, 0x31];
        //new_data.extend((to.len() as u8).to_le_bytes());
        new_data.resize(8, 0);
        new_data.extend(to);
        new_data
    }

    fn attach_bt_prelude(to: Vec<u8>) -> Vec<u8> {
        to
    }

    fn next_subcommand_id(&self) -> u8 {
        let last = self.subcommand_id.fetch_add(1, Ordering::Relaxed);
        if last == 0xF {
            self.subcommand_id.store(0, Ordering::Relaxed);
            return self.next_subcommand_id()
        }
        last + 1
    }

    fn send_subcommand<
        R : ControllerSubCommandResponse<C>,
        C : ControllerSubCommand<R>,
    >(&self, subcommand: C) -> Rc<R> {
        let mut cmd_bytes = Vec::new();
        if !self.is_bluetooth {
            cmd_bytes.push(0x01);
        }
        cmd_bytes.push(self.next_subcommand_id());
        cmd_bytes.extend(RUMBLE_BASE);
        cmd_bytes.extend(subcommand.command_bytes());

        if !self.is_bluetooth {
            cmd_bytes = Self::attach_usb_prelude(cmd_bytes);
        } else {
            cmd_bytes = Self::attach_bt_prelude(cmd_bytes);
        }
        //println!("Send subcommand {:?}", cmd_bytes.hex_dump());
        let response = self.exchange(cmd_bytes);
        //println!("Receive subcommand response {:?}", response.hex_dump());

        if self.is_bluetooth {
            Rc::new(R::from_bytes(&subcommand, response))
        } else {
            // We need to saw off 10 bytes so that we get the proper response
            let response = response[10..].to_vec();
            Rc::new(R::from_bytes(&subcommand, response))
        }
    }

    fn read_spi(&self, offset: u32, length: u8) -> Rc<SPIReadResponse> {
        for _ in 0..40 {
            let response = self.send_subcommand(SPIRead::new(offset, length));
            if response.data.len() > 0 {
                return response;
            }
            thread::sleep(Duration::from_micros(10));
        }
        panic!("Failed to do SPI read: read error or timeout");
    }

    fn write_spi(&self, offset: u32, data: Vec<u8>) {
        for _ in 0..10 {
            let response = self.send_subcommand(SPIWrite::new(offset, data.clone()));
            if response.success {
                // Introduce a little delay so we don't overwhelm the controller
                thread::sleep(Duration::from_millis(200));
                // Now read it back and check if the data matches
                let new_data = self.read_spi(offset, data.len() as u8);
                if new_data.data == data {
                    return;
                }
                println!("Write mismatch");
            }
            thread::sleep(Duration::from_micros(10));
        }
        panic!("Failed to do SPI write: write error or timeout");
    }

    fn enable_vibration(&self) {
        self.send_subcommand(VibrationEnable::enable());
    }

    fn enable_imu_data(&self) {
        self.send_subcommand(IMUDataEnable::enable());
    }

    fn increase_bt_data_rate(&self) {
        self.send_subcommand(BluetoothDataRateIncrease);
    }

    fn get_mac_address(&self) -> String {
        let mut mac_response = self.exchange(vec![0x80, 0x01]);
        mac_response.resize(10, 0);
        let mac_bytes = mac_response[4..4+6].to_vec();
        let mut mac = String::with_capacity(17);
        mac_bytes.iter().enumerate()
            .for_each(|(i, byte)| {
                mac.push_str(format!("{:02x}", byte).as_str());
                if i != mac_bytes.len() - 1 {
                    mac.push(':');
                }
            });
        mac
    }

    pub fn read_serial_number(&self) -> String {
        let serial_number = self.read_spi(0x6002, 14);
        if serial_number.data[0] == 0xff {
            "<Unavailable>".into()
        } else {
            String::from_utf8(serial_number.data.clone()).unwrap()
        }
    }

    pub fn read_colors(&self) -> ControllerColors {
        const BYTES_PER_COLOR: u8 = 3;
        // Body, Sticks/Buttons, Left Grip, Right Grip
        const PART_COUNT: u8 = 4;
        const TOTAL_COLOR_COUNT: u8 = BYTES_PER_COLOR * PART_COUNT;

        let color_response = self.read_spi(0x6050, TOTAL_COLOR_COUNT);
        ControllerColors::from_bytes(self.controller_type(), color_response.data.clone())
    }

    pub fn write_colors(&self, colors: ControllerColors) {
        self.write_spi(0x6050, colors.pack_to_bytes());
        if self.controller_type() == ControllerType::ProController {
            // Enable custom colors for everything.
            // See: https://github.com/CTCaer/jc_toolkit/issues/28#issuecomment-572569713
            // 0 = no custom colors
            // 1 = only body/sticks
            // 2 = body, sticks, grips
            self.write_spi(0x601b, vec![2]);
        }
    }

    pub fn start_usb_session(&self) {
        self.exchange(vec![0x80, 0x02]);
    }

    pub fn increase_usb_data_rate(&self) {
        self.exchange(vec![0x80, 0x03]);
    }

    #[allow(dead_code)]
    pub fn disable_usb_timeout(&self) {
        self.exchange(vec![0x80, 0x04]);
    }

    // This could be useful later
    #[allow(dead_code)]
    pub fn reset(&self) {
        self.exchange(vec![0x80, 0x06]);
    }

    pub fn has_initialized(&self) -> bool {
        self.has_initialized.load(Ordering::Relaxed)
    }

    pub fn init(&self) {
        if !self.has_initialized() {
            if !self.is_bluetooth {
                self.get_mac_address();
                self.increase_usb_data_rate();
                self.start_usb_session();
                //self.disable_usb_timeout();
            }
            self.enable_vibration();
            self.enable_imu_data();
            if self.is_bluetooth {
                self.increase_bt_data_rate();
            }
        }
    }

    pub fn deinit(&self) {
        if self.has_initialized() {
            if !self.is_bluetooth {
                self.exchange(vec![0x80, 0x05]);
            }
        }
    }

    pub fn spi_data(&self) -> SPIDumpIterator {
        // ~4 Mb https://github.com/dekuNukem/Nintendo_Switch_Reverse_Engineering#spi-peripherals
        const SPI_SIZE: u32 = 500 * 1000;
        SPIDumpIterator {
            current_offset: 0,
            max_offset: SPI_SIZE,
            controller: self.clone(),
         }
    }

}

impl Drop for Controller {
    fn drop(&mut self) {
        self.deinit();
    }
}

pub struct SPIDumpIterator {
    current_offset: u32,
    max_offset: u32,
    controller: Controller,
}

impl Iterator for SPIDumpIterator {
    type Item = Vec<u8>;
    fn next(&mut self) -> Option<Self::Item> {
        if self.current_offset < self.max_offset {
            const READ_SIZE: u8 = 0x10;
            if self.current_offset == 0 {
                // Do not start at 0
                self.current_offset = READ_SIZE as u32;
            }
            let bytes = self.controller.read_spi(self.current_offset, READ_SIZE).data.clone();
            self.current_offset += bytes.len() as u32;
            Some(bytes)
        } else {
            None
        }
    }
}

pub struct ControllersApi {
    hidapi: Rc<HidApi>
}

impl ControllersApi {

    pub fn new() -> Self {
        Self {
            hidapi: match HidApi::new() {
                Ok(api) => Rc::new(api),
                Err(e) => {
                    eprintln!("Error: {}", e);
                    panic!("Can't use hidapi");
                },
            }
        }
    }

    pub fn find_controllers(&self) -> Vec<Controller> {
        self.hidapi.device_list()
            .filter(|device| device.vendor_id() == VENDOR_ID)
            .filter(|device| ControllerType::all().iter().any(|&c| c as u16 == device.product_id()))
            .filter(|device| match device.bus_type() { BusType::Bluetooth | BusType::Usb => true, _ => false })
            .map(|device| Controller::from(device, self.hidapi.clone()))
            .collect()
    }

}
