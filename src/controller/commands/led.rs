use crate::controller::{ControllerCommand, ControllerCommandResponse};

pub struct LEDToggle;
pub struct LEDToggleResponse;

impl ControllerCommandResponse<LEDToggle> for LEDToggleResponse {
    fn from_bytes(_: &LEDToggle, _: Vec<u8>) -> Self {
        Self
    }
}

impl ControllerCommand<LEDToggleResponse> for LEDToggle {
    fn command_bytes(&self) -> Vec<u8> {
        vec![0x80]
    }
}
