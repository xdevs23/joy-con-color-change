use crate::controller::{ControllerSubCommand, ControllerSubCommandResponse};

pub struct VibrationEnable {
    enable: bool
}

impl VibrationEnable {
    pub fn enable() -> Self {
        Self { enable: true }
    }
}

pub struct VibrationEnableResponse;

impl ControllerSubCommandResponse<VibrationEnable> for VibrationEnableResponse {
    fn from_bytes(_: &VibrationEnable, _: Vec<u8>) -> Self {
        Self
    }
}

impl ControllerSubCommand<VibrationEnableResponse> for VibrationEnable {
    fn command_bytes(&self) -> Vec<u8> {
        vec![
            // Vibration Enable command
            0x48,
            self.enable as u8
        ]
    }
}
