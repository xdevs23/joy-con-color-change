use crate::controller::{ControllerSubCommand, ControllerSubCommandResponse};

pub struct IMUDataEnable {
    enable: bool
}

impl IMUDataEnable {
    pub fn enable() -> Self {
        Self { enable: true }
    }
}

pub struct IMUDataEnableResponse;

impl ControllerSubCommandResponse<IMUDataEnable> for IMUDataEnableResponse {
    fn from_bytes(_: &IMUDataEnable, _: Vec<u8>) -> Self {
        Self
    }
}

impl ControllerSubCommand<IMUDataEnableResponse> for IMUDataEnable {
    fn command_bytes(&self) -> Vec<u8> {
        vec![
            // IMU Data Enable command
            0x40,
            self.enable as u8
        ]
    }
}
