use crate::controller::{ControllerSubCommandResponse, ControllerSubCommand};


pub struct BluetoothDataRateIncrease;
pub struct BluetoothDataRateIncreaseResponse;

impl ControllerSubCommandResponse<BluetoothDataRateIncrease> for BluetoothDataRateIncreaseResponse {
    fn from_bytes(_: &BluetoothDataRateIncrease, _: Vec<u8>) -> Self {
        Self
    }
}

impl ControllerSubCommand<BluetoothDataRateIncreaseResponse> for BluetoothDataRateIncrease {
    fn command_bytes(&self) -> Vec<u8> {
        vec![
            // Bluetooth Data Rate Increase command
            0x03,
            // Enable
            0x31
        ]
    }
}
