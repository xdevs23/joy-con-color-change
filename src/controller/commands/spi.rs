use crate::controller::{ControllerSubCommandResponse, ControllerSubCommand};

pub struct SPIRead {
    offset: u32,
    length: u8
}

impl SPIRead {
    pub fn new(offset: u32, length: u8) -> Self {
        Self {
            offset, length
        }
    }
}

pub struct SPIReadResponse {
    pub data: Vec<u8>,
}

impl ControllerSubCommandResponse<SPIRead> for SPIReadResponse {
    fn from_bytes(command: &SPIRead, bytes: Vec<u8>) -> Self {
        //println!("spi read receive: {:?}", bytes.hex_dump());
        let read_length = bytes[0x13] as usize;
        if read_length != command.length as usize {
            return Self { data: vec![] }
        }
        Self { data: bytes[0x14..0x14 + read_length].to_vec() }
    }
}

impl ControllerSubCommand<SPIReadResponse> for SPIRead {
    fn command_bytes(&self) -> Vec<u8> {
        let mut cmd = Vec::new();
        // Command number
        cmd.push(0x10);
        cmd.extend(self.offset.to_le_bytes());
        cmd.push(self.length);
        // This command should be filled with zeroes up to byte 39
        cmd.resize(39, 0);
        cmd
    }
}

pub struct SPIWrite {
    offset: u32,
    data: Vec<u8>
}

pub struct SPIWriteResponse {
    pub success: bool
}

impl SPIWrite {
    pub fn new(offset: u32, data: Vec<u8>) -> Self {
        Self { offset, data }
    }
}

impl ControllerSubCommandResponse<SPIWrite> for SPIWriteResponse {
    fn from_bytes(_: &SPIWrite, bytes: Vec<u8>) -> Self {
        let command_num = bytes[0x10] as usize;
        let check_byte = bytes[0];
        if command_num != 0x11 && check_byte != 0x21  {
            return Self { success: false }
        }
        Self { success: true }
    }
}

impl ControllerSubCommand<SPIWriteResponse> for SPIWrite {
    fn command_bytes(&self) -> Vec<u8> {
        let mut cmd = Vec::new();
        // Command number
        cmd.push(0x11);
        cmd.extend(self.offset.to_le_bytes());
        assert!(self.data.len() <= u8::MAX as usize);
        cmd.push(self.data.len() as u8);
        cmd.extend(self.data.clone());
        // This command should be filled with zeroes up to byte 39
        cmd.resize(39, 0);
        cmd
    }
}

