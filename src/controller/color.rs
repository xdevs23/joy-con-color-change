use core::fmt;
use std::fmt::Display;

use colors_transform::{Rgb, Color as ColorsTransformColor};

use super::ControllerType;


#[derive(Copy, Clone, Debug)]
pub struct Color {
    pub red: u8,
    pub green: u8,
    pub blue: u8
}

impl Color {
    pub fn pack_to_bytes(&self) -> Vec<u8> {
        vec![self.red, self.green, self.blue]
    }

    pub fn from_bytes(bytes: Vec<u8>) -> Self {
        Self {
            red: bytes[0],
            green: bytes[1],
            blue: bytes[2]
        }
    }

    pub fn from_hex_code(hex: &str) -> Option<Self> {
        if let Ok(rgb) = Rgb::from_hex_str(hex) {
            Some(Self {
                red: rgb.get_red().round() as u8,
                green: rgb.get_green().round() as u8,
                blue: rgb.get_blue().round() as u8,
            })
        } else {
            None
        }
    }

    #[allow(dead_code)]
    pub fn new(red: u8, green: u8, blue: u8) -> Self {
        Self { red, green, blue }
    }
}

impl Display for Color {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(
            Rgb::from(self.red as f32, self.green as f32, self.blue as f32).to_css_hex_string().as_str()
        )
    }
}

#[derive(Copy, Clone)]
pub struct ControllerColors {
    pub body: Color,
    pub sticks: Color,
    pub left_grip: Color,
    pub right_grip: Color,
    pub controller_type: ControllerType,
}

impl ControllerColors {
    pub fn pack_to_bytes(&self) -> Vec<u8> {
        let mut packed = Vec::new();
        packed.extend(self.body.pack_to_bytes());
        packed.extend(self.sticks.pack_to_bytes());
        if self.controller_type == ControllerType::ProController {
            packed.extend(self.left_grip.pack_to_bytes());
            packed.extend(self.right_grip.pack_to_bytes());
            // In the SPI of the Pro Controller there is this one zero byte
            // Set it, just in case.
            packed.push(0);
        }
        packed
    }

    pub fn from_bytes(controller_type: ControllerType, bytes: Vec<u8>) -> Self {
        Self {
            body: Color::from_bytes(bytes[..3].to_vec()),
            sticks: Color::from_bytes(bytes[3..6].to_vec()),
            left_grip: Color::from_bytes(bytes[6..9].to_vec()),
            right_grip: Color::from_bytes(bytes[9..12].to_vec()),
            controller_type,
        }
    }

    pub fn new(
        body: Color,
        sticks: Color,
        left_grip: Color,
        right_grip: Color,
        controller_type: ControllerType,
    ) -> Self {
        Self {
            body, sticks, left_grip, right_grip, controller_type
        }
    }
}

impl Display for ControllerColors {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!(
            "Body: {}, Sticks/Buttons: {}",
            self.body, self.sticks
        ))?;
        if self.controller_type == ControllerType::ProController {
            f.write_fmt(format_args!(
                ", Left Grip: {}, Right Grip: {}",
                self.left_grip, self.right_grip
            ))?;
        }
        fmt::Result::Ok(())
    }
}
